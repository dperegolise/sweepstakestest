from django.conf.urls import include, url
from django.contrib import admin
from sweepstakes import views

import analytics

from sweepstakes_test.config import *

analytics.debug = True
analytics.write_key = SEGMENT_WRITE_KEY # TODO finish working on segment.

urlpatterns = [
    # Examples:
    # url(r'^$', 'sweepstakes_test.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^category/(?P<cat_name>.+)$', views.category),
    url(r'^$', views.index, name="home"),
    url(r'^categories/', views.categories, name="categories"),
    url(r'^profile/', views.profile),
    url(r'^logout/', views.logout, name="logout"),
    url(r'^post-sweepstake/$', views.post_sweepstake, name="post-sweepstake"),
    url(r'^watched-sweepstakes/$', views.watched_sweepstakes, name="watched-sweepstakes"),
    url(r'^watch-sweepstake/(?P<sweepstakes_id>\d+)', views.watch_sweepstake, name="add-sweepstakes"),
    url(r'^share-sweepstake-twitter/(?P<sweepstakes_id>\d+)', views.share_sweepstake_twitter, name="share-sweepstakes-twitter"),
    url(r'^remove-sweepstake/(?P<sweepstakes_id>\d+)', views.remove_sweepstake, name="remove-sweepstake"),
    url(r'^modal/(?P<sweepstakes_id>\d+)', views.modal),
    url(r'^share-modal/(?P<sweepstakes_id>\d+)', views.share_modal),
    url(r'^sweepstake/(?P<sweepstakes_id>\d+)', views.view_sweepstake),
    url(r'^search/(?P<search_query>[#]*.*)', views.search),
]
