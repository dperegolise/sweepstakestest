#Import the necessary methods from tweepy library
import django
django.setup()

import cgi
import json
import re
from pprint import pprint
from string import capitalize, upper

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

from sweepstakes_test import settings
from sweepstakes.models import Sweepstakes
from sweepstakes.models import Keyword
from sweepstakes.models import twitter_user
from sweepstakes.models import Category
from utils import *

#Variables that contains the user credentials to access Twitter API 
access_token = "3237060676-M76vemwLe5GS9GSfwjkAna4y1HESsCW7vZ6Quo3"
access_token_secret = "LPIHW96ksrGmBmpA0druYfmMpNxPPpjpaIPNlT1zDerg0"
consumer_key = "tXeW6ZlFt5IhGh3UgiDqxBSwW"
consumer_secret = "Ma8abForritiXUzikjLnJ1XzukrfzbvASqrgHtqOl5RiROhIHb"


#This is a basic listener that just prints received tweets to stdout.
class StdOutListener(StreamListener):

    def on_data(self, data):
        runJSONInsert(data)
        return True

    def on_error(self, status):
        print status

def runJSONInsert(data):
    data = json.loads(data)
    s_obj = data;
    print json.dumps(s_obj)
    print '\n'
    # Grab keywords for later 
    keywords = Keyword.objects.all()

    # Check if sweepstakes already exists
    try:
        text = find_json_value(s_obj, ['retweeted_status', 'text'], ['text'], None).encode('unicode-escape')
    except AttributeError:
        return
    dup_query_string = get_dup_query_string(text)
    if sweepstakes_exists(dup_query_string):
        return None
    if sweepstakes_exists(text):
        return None
    if sweepstakes_exists(text[:10]):
        return None
    if sweepstakes_exists(text[-10:]):
        return None

    u = get_twitter_user_obj(s_obj)
    s = get_sweepstakes_obj(u, s_obj, text)

    u.save()
    s.user_id = u;
    s.save()
    
    # Add categories to sweepstakes based on keywords
    key_found = False
    for keyword in keywords:
        if re.search('(?<=(\s|[!@#$%^&*()_+]))' + keyword.keyword_name +
                     '(?=(\s|[!@#$%^&*()_+]|s))', 
                      text, re.IGNORECASE) is not None:
            key_found = True
            categories = Category.objects.filter(keywords=keyword)
            for cat in categories:
                s.categories.add(cat)


if __name__ == '__main__':

    #This handles Twitter authetification and the connection to Twitter Streaming API
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)

    #This line filter Twitter Streams to capture data by the keywords: 'python', 'javascript', 'ruby'
    try:
        stream.filter(track=['enter to win', 'chance to win', 'I entered sweepstakes'])
    except AttributeError:
        pass
