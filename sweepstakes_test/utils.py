from datetime import datetime
import re

from sweepstakes.models import Sweepstakes
from sweepstakes.models import twitter_user

import timezones

def sweepstakes_exists(text):
    existing_sweepstakes = Sweepstakes.objects.filter(text__contains=text)
    return existing_sweepstakes


def get_dup_query_string(text):
    split_str = "((https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)|@\w+(\s|\W))"
    split = re.split(split_str, text)
    if split is None:
        return text
    else:
        return max(split, key=get_lenth_nonesafe)


def get_lenth_nonesafe(s):
    if s is None:
        return 0
    else:
        return len(s)

def find_json_value(data, defaultPath, fallbackPath, emptyVal):
    obj = data
    try:
        for path in defaultPath:
            obj = obj[path]
        return obj
    except (IndexError, KeyError):
        obj = data
        try:
            for path in fallbackPath:
                obj = obj[path]
            return obj
        except (IndexError, KeyError):
            return emptyVal


def find_json_value_or_empty(data, defaultPath, emptyVal):
    obj = data
    try:
        for path in defaultPath:
            obj = obj[path]
        return obj
    except (IndexError, KeyError):
        return emptyVal


def get_twitter_user_obj(s_obj):
    screen_name = s_obj['user']['screen_name'].encode('unicode-escape')
    try:
        return twitter_user.objects.get(screen_name=screen_name)
    except twitter_user.DoesNotExist:
         # User Details
        user_id = s_obj['user']['id']
        name = s_obj['user']['name'].encode('unicode-escape')
        followers_count = s_obj['user']['followers_count']
        profile_image_url = s_obj['user']['profile_image_url']
        return twitter_user(user_id=user_id, 
                            name=name, screen_name=screen_name, 
                            followers_count=followers_count,
                            profile_image_url = profile_image_url)

def get_sweepstakes_from_tweet(status, sweepstakes_link=None, user_id=None):
    sweepstakes_image_url = None
    twitter_posted_at = str(datetime.now())
    twitter_post_id = status.id
    if len(status.media) > 0:
      sweepstakes_image_url = status.media[0]['media_url']
    if sweepstakes_link is None:
      sweepstakes_link = "https://twitter.com/GrassAttic/status/" + str(twitter_post_id)
    retweeted_count = 0
    favorited_count = 0
    return Sweepstakes(twitter_posted_at=twitter_posted_at, 
                       text=status.text, twitter_post_id=twitter_post_id, 
                       sweepstakes_image_url=sweepstakes_image_url,
                       retweeted_count = retweeted_count,
                       favorited_count = favorited_count,
                       sweepstakes_link = sweepstakes_link, user_id=user_id)

def get_sweepstakes_obj(u_obj, s_obj, text):
    # Post details
    twitter_posted_at = datetime.fromtimestamp(int(s_obj['timestamp_ms'])/1000.0, timezones.utc);
    twitter_post_id = s_obj['id'];

    sweepstakes_image_url = find_json_value_or_empty(s_obj, 
                                                     ['entities', 
                                                      'media',
                                                      0,
                                                      'media_url'], 
                                                     None)

    retweeted_count = find_json_value(s_obj, 
                                      ['retweeted_status', 
                                       'retweet_count'], 
                                      ['retweet_count'], 0)

    favorited_count = find_json_value(s_obj, 
                                      ['retweeted_status', 
                                       'favorite_count'], 
                                      ['favorite_count'], 0)

    sweepstakes_link = find_json_value(s_obj, 
                                       ['entities', 
                                        'urls',
                                        0,
                                        'expanded_url'], 
                                       ['entities',
                                        'media',
                                        0,
                                        'expanded_url'],
                                       'https://twitter.com/' + u_obj.screen_name + 
                                        '/' + str(twitter_post_id))

    return Sweepstakes(twitter_posted_at=twitter_posted_at, 
                       text=text, twitter_post_id=twitter_post_id, 
                       sweepstakes_image_url=sweepstakes_image_url,
                       retweeted_count = retweeted_count,
                       favorited_count = favorited_count,
                       sweepstakes_link = sweepstakes_link)