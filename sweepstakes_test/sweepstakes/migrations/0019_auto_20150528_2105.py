# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0018_auto_20150526_1820'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sweepstakes',
            old_name='text',
            new_name='_text',
        ),
        migrations.RenameField(
            model_name='twitter_user',
            old_name='name',
            new_name='_name',
        ),
        migrations.RenameField(
            model_name='twitter_user',
            old_name='screen_name',
            new_name='_screen_name',
        ),
    ]
