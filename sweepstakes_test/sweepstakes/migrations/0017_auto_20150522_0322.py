# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0016_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='twitterprofile',
            name='user',
        ),
        migrations.AddField(
            model_name='twitter_user',
            name='watched_sweepstakes',
            field=models.ManyToManyField(to='sweepstakes.Sweepstakes'),
        ),
        migrations.DeleteModel(
            name='TwitterProfile',
        ),
    ]
