# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0009_auto_20150517_0014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sweepstakes',
            name='user_id',
            field=models.ForeignKey(to='sweepstakes.twitter_user', null=True),
        ),
    ]
