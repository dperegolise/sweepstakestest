# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0010_auto_20150517_0018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sweepstakes',
            name='sweepstakes_image_url',
            field=models.URLField(null=True, blank=True),
        ),
    ]
