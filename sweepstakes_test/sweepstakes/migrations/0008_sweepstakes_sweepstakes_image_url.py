# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0007_auto_20150516_0332'),
    ]

    operations = [
        migrations.AddField(
            model_name='sweepstakes',
            name='sweepstakes_image_url',
            field=models.URLField(default=''),
            preserve_default=False,
        ),
    ]
