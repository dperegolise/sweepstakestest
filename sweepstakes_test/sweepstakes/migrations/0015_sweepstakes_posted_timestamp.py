# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0014_auto_20150517_0214'),
    ]

    operations = [
        migrations.AddField(
            model_name='sweepstakes',
            name='posted_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 22, 3, 46, 0, 360237, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
