# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0002_sweepstakes_sweepstakes_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='sweepstakes',
            name='poster_keyname',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
