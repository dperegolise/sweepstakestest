# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0015_sweepstakes_posted_timestamp'),
        ('sweepstakes', '0015_twitterprofile'),
    ]

    operations = [
    ]
