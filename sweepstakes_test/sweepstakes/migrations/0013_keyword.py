# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0012_auto_20150517_0117'),
    ]

    operations = [
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('keyword_name', models.CharField(max_length=45, null=True, blank=True)),
                ('categories', models.ManyToManyField(to='sweepstakes.Category')),
            ],
        ),
    ]
