# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0008_sweepstakes_sweepstakes_image_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='sweepstakes',
            name='favorited_count',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sweepstakes',
            name='retweeted_count',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sweepstakes',
            name='user_id',
            field=models.ForeignKey(default=None, to='sweepstakes.twitter_user'),
            preserve_default=False,
        ),
    ]
