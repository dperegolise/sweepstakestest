# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0006_auto_20150516_0331'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sweepstakes',
            name='poster_desc',
        ),
        migrations.RemoveField(
            model_name='sweepstakes',
            name='poster_screen_name',
        ),
    ]
