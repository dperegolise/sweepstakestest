# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sweepstakes',
            name='sweepstakes_link',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
    ]
