# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0013_keyword'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='keyword',
            name='categories',
        ),
        migrations.AddField(
            model_name='category',
            name='keywords',
            field=models.ManyToManyField(to='sweepstakes.Keyword'),
        ),
    ]
