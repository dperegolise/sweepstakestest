# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0005_twitter_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sweepstakes',
            old_name='poster_keyname',
            new_name='poster_screen_name',
        ),
    ]
