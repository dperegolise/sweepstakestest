# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0003_sweepstakes_poster_keyname'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sweepstakes',
            name='twitter_post_id',
            field=models.BigIntegerField(null=True, blank=True),
        ),
    ]
