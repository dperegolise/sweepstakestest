# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0011_auto_20150517_0033'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Categories',
            new_name='Category',
        ),
        migrations.RemoveField(
            model_name='categoriessweepstakes',
            name='category',
        ),
        migrations.RemoveField(
            model_name='categoriessweepstakes',
            name='sweepstakes',
        ),
        migrations.AddField(
            model_name='sweepstakes',
            name='categories',
            field=models.ManyToManyField(to='sweepstakes.Category'),
        ),
        migrations.DeleteModel(
            name='CategoriesSweepstakes',
        ),
    ]
