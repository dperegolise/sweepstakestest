# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0004_auto_20150507_1616'),
    ]

    operations = [
        migrations.CreateModel(
            name='twitter_user',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('user_id', models.IntegerField()),
                ('name', models.CharField(max_length=20, null=True, blank=True)),
                ('screen_name', models.CharField(max_length=20, null=True, blank=True)),
                ('followers_count', models.IntegerField()),
                ('profile_image_url', models.URLField()),
            ],
        ),
    ]
