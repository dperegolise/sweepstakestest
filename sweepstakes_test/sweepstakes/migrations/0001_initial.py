# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('category_name', models.CharField(max_length=45, null=True, blank=True)),
                ('category_desc', models.CharField(max_length=1024, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='CategoriesSweepstakes',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('category', models.ForeignKey(blank=True, to='sweepstakes.Categories', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sweepstakes',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('twitter_posted_at', models.DateTimeField(null=True, blank=True)),
                ('text', models.CharField(max_length=140, null=True, blank=True)),
                ('twitter_post_id', models.IntegerField(null=True, blank=True)),
                ('poster_desc', models.CharField(max_length=1024, null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='categoriessweepstakes',
            name='sweepstakes',
            field=models.ForeignKey(blank=True, to='sweepstakes.Sweepstakes', null=True),
        ),
    ]
