# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0017_auto_20150522_0322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sweepstakes',
            name='text',
            field=models.CharField(max_length=240, null=True, blank=True),
        ),
    ]
