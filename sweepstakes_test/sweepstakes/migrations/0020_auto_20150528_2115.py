# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sweepstakes', '0019_auto_20150528_2105'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sweepstakes',
            old_name='_text',
            new_name='text',
        ),
        migrations.RenameField(
            model_name='twitter_user',
            old_name='_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='twitter_user',
            old_name='_screen_name',
            new_name='screen_name',
        ),
    ]
