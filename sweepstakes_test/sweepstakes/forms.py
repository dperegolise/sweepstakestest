# In forms.py...
from django import forms

class PostTweetForm(forms.Form):
    message = forms.CharField(max_length=140)
    tweet_photo = forms.FileField()