import re

from django.db import models

# Create your models here.

class Keyword(models.Model):
    id = models.AutoField(primary_key=True)
    keyword_name = models.CharField(max_length=45, blank=True, null=True)

    def __unicode__(self):
        return self.keyword_name

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=45, blank=True, null=True)
    category_desc = models.CharField(max_length=1024, blank=True, null=True)
    keywords = models.ManyToManyField('Keyword')

    def __unicode__(self):
        return self.category_name

class Sweepstakes(models.Model):
    id = models.AutoField(primary_key=True)
    twitter_posted_at = models.DateTimeField(blank=True, null=True)
    text = models.CharField(max_length=240, blank=True, null=True)
    twitter_post_id = models.BigIntegerField(blank=True, null=True)
    sweepstakes_link = models.CharField(max_length=1024, blank=True, null=True)
    sweepstakes_image_url = models.URLField(blank=True, null=True)
    retweeted_count = models.IntegerField()
    favorited_count = models.IntegerField()
    user_id = models.ForeignKey('twitter_user', null=True)
    categories = models.ManyToManyField(Category)
    posted_timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.text.decode('unicode-escape')

    def get_text(self):
        try:
            return self.text.decode('unicode-escape')
        except UnicodeDecodeError:
            return re.sub('\\U.+', '', self.text)

    text_unicode = property(get_text)

class twitter_user(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    name = models.CharField(max_length=20, blank=True, null=True)
    screen_name = models.CharField(max_length=20, blank=True, null=True)
    followers_count = models.IntegerField()
    profile_image_url = models.URLField()
    watched_sweepstakes = models.ManyToManyField('Sweepstakes')

    def __unicode__(self):
        return self.screen_name.decode('unicode-escape')

    def get_name(self):
        try:
            return self.name.decode('unicode-escape')
        except UnicodeDecodeError:
            return self.name

    name_unicode = property(get_name)

    def get_screen_name(self):
        try:
            return self.screen_name.decode('unicode-escape')
        except UnicodeDecodeError:
            return self.screen_name

    screen_name_unicode = property(get_screen_name)
