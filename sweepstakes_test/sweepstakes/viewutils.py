from datetime import datetime
from datetime import timedelta

def get_interval_query(request, query):
    date_filter = request.GET.get('interval', '')
    if date_filter == "today":
        timestamp_from = datetime.now().date()
        timestamp_to = datetime.now().date() + timedelta(days=1)
    if date_filter == "week":
        timestamp_from = datetime.now().date() - timedelta(days=7)
        timestamp_to = datetime.now().date() + timedelta(days=1)
    if date_filter == "month":
        timestamp_from = datetime.now().date() - timedelta(days=30)
        timestamp_to = datetime.now().date() + timedelta(days=1)
    return query.filter(posted_timestamp__gte=timestamp_from,
                             posted_timestamp__lt=timestamp_to)

def get_sorting_query(request, query):
    sort_filter = request.GET.get('sorting', '')
    if sort_filter == "followers":
        query = query.order_by('-user_id__followers_count')
    if sort_filter == "retweets":
        query = query.order_by('-retweeted_count')
    if sort_filter == "favorites":
        query = query.order_by('-favorited_count')
    if sort_filter == "time":
        query = query.order_by('-twitter_posted_at')
    return query

def image_exist(path):
    r = requests.head(path)
    return r.status_code == requests.codes.ok