# -*- coding: utf-8 -*-

import cgi
from datetime import datetime
from datetime import timedelta
import json
import re
import twitter
from urlparse import parse_qs
from pprint import pprint
from string import capitalize, upper
from django.db import models
from django.http import HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import logout as auth_logout
from .models import Category
from .models import Sweepstakes
from .models import twitter_user
from .models import Keyword

from sweepstakes_test.config import *
from utils import *

import analytics

import viewutils

# Create your views here.
def index(request,template='sweepstakes/index.html', page_template='sweepstakes/tweets.html'):
    query = sweepstakes = Sweepstakes.objects

    if request.GET.get('interval'):
        query = viewutils.get_interval_query(request, query)
    if not request.GET.get('interval'):
        query = query.all()
    if request.GET.get('sorting'):
        query = viewutils.get_sorting_query(request, query)
    if not request.GET.get('sorting'):
        query = query.order_by('-user_id__followers_count')
    timestamp_from = datetime.now().date() - timedelta(days=30)
    timestamp_to = datetime.now().date() + timedelta(days=1)
    sweepstakes = query.filter(posted_timestamp__gte=timestamp_from,
                             posted_timestamp__lt=timestamp_to)
    # categories = Category.objects.order_by('category_name')
    categories = Category.objects \
                         .all() \
                         .annotate(num_entries=models.Count('sweepstakes')) \
                         .order_by('category_name')
    user = None
    userId = 'userId'
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
                userId = request.user.id
        except twitter_user.DoesNotExist:
                pass
    watched_ids = None
    try:
        watched_ids = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.all().values_list('pk', flat=True)
    except twitter_user.DoesNotExist:
            pass
    context = {
        'categories': categories,
        'sweepstakes': sweepstakes,
        'page_template': page_template,
        'user': user,
        'watched_ids': watched_ids,
        'current_category': 'The Largest Directory Of Free Twitter Contests & Sweepstakes'
    }
    if request.is_ajax():
        template = page_template
    analytics.page(userId, 'Viewed Index page')
    return render_to_response(template, context, context_instance=RequestContext(request))

def about(request, template='sweepstakes/about.html'):
    user = None
    userId = 'userId'
    categories = Category.objects \
                     .all() \
                     .annotate(num_entries=models.Count('sweepstakes')) \
                     .order_by('category_name')
    if request.user.is_authenticated():
        user = twitter_user.objects.get(user_id=request.user.id)
        userId = request.user.id
    analytics.page(userId, 'Viewed About us page')
    return render(request, template, \
                  {'current_category': 'About us', 'user': user, 'categories': categories})

def watched_sweepstakes(request,template='sweepstakes/watched_sweepstakes.html', page_template='sweepstakes/tweets.html'):
    user = None
    userId = 'userId'
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
                userId = request.user.id
        except twitter_user.DoesNotExist:
                user = None
    watched_ids = None
    try:
        watched_ids = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.all().values_list('pk', flat=True)
    except twitter_user.DoesNotExist:
            pass
    sweepstakes = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.all()
    categories = Category.objects.all()
    context = {
        'categories': categories,
        'sweepstakes': sweepstakes,
        'page_template': page_template,
        'user': user,
        'watched_ids': watched_ids,
        'current_category': 'Watched Sweepstakes'
    }
    if request.is_ajax():
        template = page_template
    analytics.page(userId, 'Viewed Watched Sweepstakes page')
    return render_to_response(template, context, context_instance=RequestContext(request))

def landing_page(request, template='sweepstakes/landing_page.html'):
    latest_sweepstakes = sweepstakes = Sweepstakes.objects.order_by('-user_id__followers_count', \
                                               '-retweeted_count', \
                                               '-favorited_count')[:1]
    context = {
        'latest_sweepstakes': latest_sweepstakes,
    }
    return render_to_response(template, context, context_instance=RequestContext(request))

def categories(request):
    user = None
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
                userId = request.user.id
        except twitter_user.DoesNotExist:
                user = None
    categories = Category.objects \
                         .all() \
                         .annotate(num_entries=models.Count('sweepstakes')) \
                         .order_by('category_name')
    context = {
        'user': user,
        'categories': categories,
        'current_category': 'Categories'
    }
    return render_to_response('sweepstakes/categories.html', context, context_instance=RequestContext(request))

def view_sweepstake(request, sweepstakes_id, template="sweepstakes/sweepstake.html"):
    user = None
    userId = 'userId'
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
                userId = request.user.id
        except twitter_user.DoesNotExist:
                user = None
    watched_ids = None
    try:
        watched_ids = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.all().values_list('pk', flat=True)
    except twitter_user.DoesNotExist:
            pass
    sweepstake = Sweepstakes.objects.get(pk=sweepstakes_id)
    categories = Category.objects.all()
    context = {
        'categories': categories,
        'sweepstake': sweepstake,
        'user': user,
        'watched_ids': watched_ids,
        'current_category': 'Sweepstake #'+sweepstakes_id
    }
    analytics.page(userId, 'Viewing sweepstakes', {'sweepstakes_id': sweepstakes_id})
    return render_to_response(template, context, context_instance=RequestContext(request))

def share_modal(request, sweepstakes_id):
    user = None
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
        except twitter_user.DoesNotExist:
                pass
    sweepstakes = Sweepstakes.objects.get(pk=sweepstakes_id)
    return render(request, 'sweepstakes/fragments/share-modal.html', \
                  {'sweepstakes': sweepstakes, 'user': user})

def modal(request, sweepstakes_id):
    user = None
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
        except twitter_user.DoesNotExist:
                pass
    watched_ids = None
    try:
        watched_ids = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.all().values_list('pk', flat=True)
    except twitter_user.DoesNotExist:
            pass
    sweepstakes = Sweepstakes.objects.get(pk=sweepstakes_id)
    return render(request, 'sweepstakes/modal.html', \
                  {'sweepstakes': sweepstakes, 'watched_ids': watched_ids, 'user': user})

def watch_sweepstake(request, sweepstakes_id):
    if request.user.is_authenticated():
        sweepstake = Sweepstakes.objects.get(pk=sweepstakes_id)
        tu = twitter_user.objects.get(user_id=request.user.id)
        tu.watched_sweepstakes.add(sweepstake)
        tu.save()
    return HttpResponse(sweepstakes_id)

def share_sweepstake_twitter(request, sweepstakes_id):
    if request.user.is_authenticated():
        user = request.user.social_auth.filter(provider='twitter', user_id=request.user.id).first()
        oauth_token = user.extra_data['access_token']['oauth_token']
        oauth_token_secret = user.extra_data['access_token']['oauth_token_secret']
        api = twitter.Api(consumer_key=SOCIAL_AUTH_TWITTER_KEY,
                    consumer_secret=SOCIAL_AUTH_TWITTER_SECRET,
                    access_token_key=oauth_token,
                    access_token_secret=oauth_token_secret)
        sweepstake = Sweepstakes.objects.get(pk=sweepstakes_id)
        tweet = str(sweepstake.text_unicode)
        status = api.PostUpdate(tweet)
    return HttpResponse(sweepstakes_id)

def remove_sweepstake(request, sweepstakes_id):
    if request.user.is_authenticated():
        sweepstake = Sweepstakes.objects.get(pk=sweepstakes_id)
        tu = twitter_user.objects.get(user_id=request.user.id)
        tu.watched_sweepstakes.remove(sweepstake)
        tu.save()
    return HttpResponse(sweepstakes_id)

def post_sweepstake(request):
	status = None
	tweet = None
	photo = None
	sweepstakes_link = None
	if request.method == 'POST' and request.user.is_authenticated():
		user = request.user.social_auth.filter(provider='twitter', user_id=request.user.id).first()
		oauth_token = user.extra_data['access_token']['oauth_token']
		oauth_token_secret = user.extra_data['access_token']['oauth_token_secret']
		if 'sweepstakes_link' in request.POST:
			sweepstakes_link = request.POST['sweepstakes_link']
		if 'message' in request.POST:
			tweet = request.POST['message']
		if 'tweet-photo' in request.FILES:
			photo =  request.FILES['tweet-photo']
		api = twitter.Api(consumer_key=SOCIAL_AUTH_TWITTER_KEY,
                    consumer_secret=SOCIAL_AUTH_TWITTER_SECRET,
                    access_token_key=oauth_token,
                    access_token_secret=oauth_token_secret)
		if photo:
			status = api.PostMedia(tweet, photo)
		else:
			status = api.PostUpdate(tweet)
		tu = twitter_user.objects.get(user_id=request.user.id)	
		s = get_sweepstakes_from_tweet(status, sweepstakes_link, tu)
		s.save()
	return redirect('home')

def category(request, cat_name, template='sweepstakes/index.html', page_template='sweepstakes/tweets.html'):
    query = sweepstakes = Sweepstakes.objects
    if request.GET.get('interval'):
        query = viewutils.get_interval_query(request, query)
    if not request.GET.get('interval'):
        query = query.all()
    if request.GET.get('sorting'):
        query = viewutils.get_sorting_query(request, query)
    if not request.GET.get('sorting'):
        query = query.order_by('-user_id__followers_count')
    timestamp_from = datetime.now().date() - timedelta(days=7)
    timestamp_to = datetime.now().date() + timedelta(days=1)    
    query = query.filter(categories__category_name=cat_name)    
    sweepstakes = query.filter(posted_timestamp__gte=timestamp_from,
                             posted_timestamp__lt=timestamp_to)
    user = None
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
        except twitter_user.DoesNotExist:
                pass                               
    categories = Category.objects \
                         .all() \
                         .annotate(num_entries=models.Count('sweepstakes')) \
                         .order_by('category_name')
    context = {
        'categories': categories,
        'sweepstakes': sweepstakes,
        'page_template': page_template,
        'user': user,
        'current_category': cat_name
    }
    if request.is_ajax():
        template = page_template
    return render_to_response(template, context, context_instance=RequestContext(request))

def logout(request):
    auth_logout(request)
    return redirect('home')

def search(request, search_query, template='sweepstakes/index.html', page_template='sweepstakes/tweets.html'):
    query = Sweepstakes.objects
    if request.GET.get('interval'):
        query = viewutils.get_interval_query(request, query)
    if request.GET.get('sorting'):
        query = viewutils.get_sorting_query(request, query)
    if not request.GET.get('sorting'):
        query = query.order_by('-user_id__followers_count', \
                                        '-retweeted_count', \
                                        '-favorited_count')
    sweepstakes = query.filter( \
                             text__icontains=search_query \
                             )
    categories = Category.objects \
                         .all() \
                         .annotate(num_entries=models.Count('sweepstakes')) \
                         .order_by('category_name')
    user = None
    if request.user.is_authenticated():
        try:
                user = twitter_user.objects.get(user_id=request.user.id)
        except twitter_user.DoesNotExist:
                pass
    context = {
        'categories': categories,
        'sweepstakes': sweepstakes,
        'page_template': page_template,
        'current_category': search_query,
        'user': user,
    }
    if request.is_ajax():
        # template = 'sweepstakes/index.html'
        template = page_template
    return render_to_response(template, context, context_instance=RequestContext(request))

def profile(request):
    categories = Category.objects.all()
    user = None
    userId = 'userId'
    if request.user.is_authenticated():
        try:
            user = twitter_user.objects.get(user_id=request.user.id)
            userId = request.user.id
        except twitter_user.DoesNotExist:
                pass
    watched_ids = None
    try:
        watched_ids = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.all().values_list('pk', flat=True)
    except twitter_user.DoesNotExist:
            pass
    watched_count = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.count()
    sweepstakes = twitter_user.objects.get(user_id=request.user.id).watched_sweepstakes.all()
    posted_sweepstakes = Sweepstakes.objects.filter(user_id__user_id=request.user.id)
    analytics.page(userId, 'Viewing profile page')
    return render(request, 'sweepstakes/profile.html', \
                  { 'posted_sweepstakes': posted_sweepstakes, 'categories': categories, 'user': user, 'watched_count':watched_count, 'sweepstakes':sweepstakes, 'current_category': 'Profile'})
