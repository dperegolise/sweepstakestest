from django.contrib import admin

# Register your models here.
from .models import Category
from .models import Sweepstakes
from .models import twitter_user
from .models import Keyword

admin.site.register(twitter_user)
admin.site.register(Category)
admin.site.register(Keyword)
admin.site.register(Sweepstakes)