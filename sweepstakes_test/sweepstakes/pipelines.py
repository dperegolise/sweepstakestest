from sweepstakes.models import twitter_user

from django.contrib.auth.signals import user_logged_in, user_logged_out

import analytics


def get_user_details(backend, user, response, details, is_new=False, *args, **kwargs):
		try:
			profile = twitter_user.objects.get(user_id = user.id)
		except twitter_user.DoesNotExist:
			profile = twitter_user(user_id = user.id)
			profile.name = response['name']
			profile.screen_name = response['name']
			profile.followers_count = response['followers_count']
			profile.profile_image_url = response['profile_image_url_https']
			profile.save()

def track_login(sender, user, request, **kwargs):
	analytics.track(request.user.id, 'User Logged In', {'userId': user.id})

def track_logout(sender, user, request, **kwargs):
	analytics.track(request.user.id, 'User Logged Out', {'userId': user.id})


user_logged_in.connect(track_login)

user_logged_out.connect(track_logout)