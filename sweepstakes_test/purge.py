import django
django.setup()

from django.utils import timezone
from datetime import timedelta

from sweepstakes.models import Sweepstakes


if __name__ == '__main__':
	days = 30
	print timezone.now()-timedelta(days=days)
	sweepstakes = Sweepstakes.objects.filter(posted_timestamp__lte=timezone.now()-timedelta(days=days)).delete()
	print "Finished purging db"